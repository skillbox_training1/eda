import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from IPython.display import display

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
df_place = pd.read_csv('kiva_mpi_region_locations.csv')
df_full_info = pd.read_csv('kiva_loans.csv')

df_full_info_1 = df_full_info[['id', 'sector', 'country', 'borrower_genders']]
df_full_info_1 = df_full_info_1.merge(df_place[['country', 'world_region']], how='left',
                                      left_on='country', right_on='country')

df_full_info_1['borrower_genders'] = [elem if elem in ['female', 'male'] else 'group' for elem in
                                      df_full_info_1['borrower_genders']]
# df_full_info_1 = df_full_info_1.groupby('country').

print(df_full_info_1.head(100))


