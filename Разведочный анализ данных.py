import numpy as np
import pandas as pd
import math
import missingno as msno
from datetime import datetime, timedelta

import matplotlib.pyplot as plt
# matplotlib inline
import seaborn as sns
plt.style.use('seaborn-darkgrid')
palette = plt.get_cmap('Set2')

import os

df_kiva_loans = pd.read_csv('kiva_loans.csv')
df_mpi = pd.read_csv('kiva_mpi_region_locations.csv')

print(df_kiva_loans.head(5))

# msno.bar(df_kiva_loans)
# msno.matrix(df_kiva_loans)
# print(plt.show())

'''print(df_kiva_loans.dtypes)
print(df_kiva_loans.describe(include=[np.number]))

countries = df_kiva_loans['country'].value_counts()[df_kiva_loans['country'].value_counts(normalize=True)>0.005]
list_countries = list(countries.index)
print(countries)
print(list_countries)
print(df_kiva_loans['country'].head(10))'''

# графический график заявок по странам. Их количество.
'''countries = df_kiva_loans['country'].value_counts()[df_kiva_loans['country'].value_counts(normalize=True)>0.005]
plt.figure(figsize=(20, 10))
plt.title('Количество займов в разрезе стран', fontsize=16)
plt.tick_params(labelsize = 25)
sns.barplot(y=countries.index, x = countries.values, alpha =0.6)
print(plt.show())'''

'''pd.set_option('display.max_columns', 20)
print(df_mpi.head())

df.mpi_groupby(['ISO', 'country', 'world_region'])['MPI'].mean().fillna(0).reset_index()
df_kiva_loans = df_kiva_loans.merge(df_mpi_grouped, how = 'left', on='country')
regions = df_kiva_loans['world_region'].value_counts()
regions_list = regions.index.to_list()

plt.figure(figsize=(20,10))
sns.barplot(y=regions.index, x = regions.values, alpha = 0.6)
plt.title('Количество займов в разрезе макрорегионов', fonsize = 18)
plt.tick_params(labelsize = 16)

print(plt.show())'''

# Количество займов в разрезе секторов
'''sectors = df_kiva_loans['sector'].value_counts()
plt.figure(figsize=(20,10))
plt.title('Количество займов в разрезе секторов', fontsize=16)
plt.tick_params(labelsize=15)

sns.barplot(y=sectors.index, x=sectors.values, alpha=0.6)
print(plt.show())'''

# Количество займов в разрезе видов деятельности
'''activities = df_kiva_loans['activity'].value_counts().head(30)
print(activities)
plt.figure(figsize=(20,10))
plt.title('Количество займов в разрезе видов деятельности', fontsize=16)
plt.tick_params(labelsize=14)
sns.barplot(x=activities.values, y=activities.index, alpha=0.6)
print(plt.show())'''

"""data = df_kiva_loans[df_kiva_loans['borrower_genders'].isin(['male'])].groupby('country')['borrower_genders'].value_counts()
print(data.head(100))
sns.barplot(data=data, x = data.values, y=data.index)
print(plt.show())"""

# Распределение суммы займа
plt.figure(figsize=(12,6))
plt.title('Распределение суммы займа', fontsize=16)
plt.tick_params(labelsize=14)
sns.histplot(df_kiva_loans['loan_amount'], axlabel=False)
print(plt.show())